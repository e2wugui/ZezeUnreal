// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ZezeUnrealGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class ZEZEUNREAL_API AZezeUnrealGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
