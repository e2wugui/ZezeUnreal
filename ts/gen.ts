// auto-generated

import { Zeze } from "zeze"

export class demo_Bean1 implements Zeze.Bean {
    public static readonly Enum1 = 4;

    public V1: number;  // bean1comm		bean1line2
    public V2: Map<number, number>;  // bean1v2		bean1v2line2


    public constructor() {
        this.V1 = 1;
        this.V2 = new Map<number, number>();
    }


    public static readonly TYPEID: bigint = -410057899348847631n;
    public TypeId(): bigint { return demo_Bean1.TYPEID; }

    public Encode(_o_: Zeze.ByteBuffer) {
        var _i_ = 0;
        {
            var _xn_ = this.V1;
            if (_xn_ != 0) {
                _i_ = _o_.WriteTag(_i_, 1, Zeze.ByteBuffer.INTEGER);
                _o_.WriteInt(_xn_);
            }
        }
        {
            var _x2_ = this.V2;
            var _n_ = _x2_.size;
            if (_n_ != 0) {
                _i_ = _o_.WriteTag(_i_, 2, Zeze.ByteBuffer.MAP);
                _o_.WriteMapType(_n_, Zeze.ByteBuffer.INTEGER, Zeze.ByteBuffer.INTEGER);
                for (let _e_ of _x2_.entries()) {
                    _o_.WriteInt(_e_[0]);
                    _o_.WriteInt(_e_[1]);
                }
            }
        }
        _o_.WriteByte(0);
    }

    public Decode(_o_: Zeze.ByteBuffer) {
        var _t_ = _o_.ReadByte();
        var _i_ = _o_.ReadTagSize(_t_);
        if (_i_ == 1) {
            this.V1 = _o_.ReadIntT(_t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        if (_i_ == 2) {
            var _x2_ = this.V2;
            _x2_.clear();
            if ((_t_ & Zeze.ByteBuffer.TAG_MASK) == Zeze.ByteBuffer.MAP) {
                var _s_ = (_t_ = _o_.ReadByte()) >> Zeze.ByteBuffer.TAG_SHIFT;
                for (var _n_ = _o_.ReadUInt(); _n_ > 0; _n_--) {
                    var _k2_ = _o_.ReadIntT(_s_);
                    var _v2_ = _o_.ReadIntT(_t_);
                    _x2_.set(_k2_, _v2_);
                }
            } else
                _o_.SkipUnknownField(_t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        while (_t_ != 0) {
            _o_.SkipUnknownField(_t_);
            _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
    }
}

export class demo_Module1_AutoValue implements Zeze.Bean {
    public Current: bigint; 
    public Name: string; 
    public LocalId: bigint; 
    public ListDynamic: Array<Zeze.DynamicBean>; 
    public MapDynamic: Map<bigint, Zeze.DynamicBean>; 


    public constructor() {
        this.Current = 0n;
        this.Name = "";
        this.LocalId = 0n;
        this.ListDynamic = new Array<Zeze.DynamicBean>();
        this.MapDynamic = new Map<bigint, Zeze.DynamicBean>();
    }


    public static readonly TYPEID: bigint = 87786109461620345n;
    public TypeId(): bigint { return demo_Module1_AutoValue.TYPEID; }

    public Encode(_o_: Zeze.ByteBuffer) {
        var _i_ = 0;
        {
            var _xl_ = this.Current;
            if (_xl_ != 0n) {
                _i_ = _o_.WriteTag(_i_, 1, Zeze.ByteBuffer.INTEGER);
                _o_.WriteLong(_xl_);
            }
        }
        {
            var _xs_ = this.Name;
            if (_xs_.length != 0) {
                _i_ = _o_.WriteTag(_i_, 2, Zeze.ByteBuffer.BYTES);
                _o_.WriteString(_xs_);
            }
        }
        {
            var _xl_ = this.LocalId;
            if (_xl_ != 0n) {
                _i_ = _o_.WriteTag(_i_, 3, Zeze.ByteBuffer.INTEGER);
                _o_.WriteLong(_xl_);
            }
        }
        {
            var _x4_ = this.ListDynamic;
            var _n_ = _x4_.length;
            if (_n_ != 0) {
                _i_ = _o_.WriteTag(_i_, 4, Zeze.ByteBuffer.LIST);
                _o_.WriteListType(_n_, Zeze.ByteBuffer.DYNAMIC);
                for (var _v_ in _x4_) {
                    this.ListDynamic[_v_].Encode(_o_);
                }
            }
        }
        {
            var _x5_ = this.MapDynamic;
            var _n_ = _x5_.size;
            if (_n_ != 0) {
                _i_ = _o_.WriteTag(_i_, 5, Zeze.ByteBuffer.MAP);
                _o_.WriteMapType(_n_, Zeze.ByteBuffer.INTEGER, Zeze.ByteBuffer.DYNAMIC);
                for (let _e_ of _x5_.entries()) {
                    _o_.WriteLong(_e_[0]);
                    _e_[1].Encode(_o_);
                }
            }
        }
        _o_.WriteByte(0);
    }

    public Decode(_o_: Zeze.ByteBuffer) {
        var _t_ = _o_.ReadByte();
        var _i_ = _o_.ReadTagSize(_t_);
        if (_i_ == 1) {
            this.Current = _o_.ReadLongT(_t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        if (_i_ == 2) {
            this.Name = _o_.ReadStringT(_t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        if (_i_ == 3) {
            this.LocalId = _o_.ReadLongT(_t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        if (_i_ == 4) {
            var _x4_ = new Array<Zeze.DynamicBean>();
            this.ListDynamic = _x4_;
            if ((_t_ & Zeze.ByteBuffer.TAG_MASK) == Zeze.ByteBuffer.LIST)
            {
                for (var _n_ = _o_.ReadTagSize(_t_ = _o_.ReadByte()); _n_ > 0; _n_--)
                {
                    var _e_ = new Zeze.DynamicBean(demo_Module1_AutoValue.GetSpecialTypeIdFromBean_4, demo_Module1_AutoValue.CreateBeanFromSpecialTypeId_4);
                    _o_.ReadDynamic(_e_, _t_);
                    _x4_.push(_e_);
                }
            }
            else
                _o_.SkipUnknownField(_t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        if (_i_ == 5) {
            var _x5_ = this.MapDynamic;
            _x5_.clear();
            if ((_t_ & Zeze.ByteBuffer.TAG_MASK) == Zeze.ByteBuffer.MAP) {
                var _s_ = (_t_ = _o_.ReadByte()) >> Zeze.ByteBuffer.TAG_SHIFT;
                for (var _n_ = _o_.ReadUInt(); _n_ > 0; _n_--) {
                    var _k5_ = _o_.ReadLongT(_s_);
                    var _v5_ = new Zeze.DynamicBean(demo_Module1_AutoValue.GetSpecialTypeIdFromBean_5, demo_Module1_AutoValue.CreateBeanFromSpecialTypeId_5);
                    _o_.ReadDynamic(_v5_, _t_);
                    _x5_.set(_k5_, _v5_);
                }
            } else
                _o_.SkipUnknownField(_t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        while (_t_ != 0) {
            _o_.SkipUnknownField(_t_);
            _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
    }
    public static GetSpecialTypeIdFromBean_4(bean: Zeze.Bean): bigint {
        switch (bean.TypeId())
        {
            case Zeze.EmptyBean.TYPEID: return Zeze.EmptyBean.TYPEID;
            case -6540229162696180765n: return -6540229162696180765n; // demo.Module1.Simple
            case -410057899348847631n: return -410057899348847631n; // demo.Bean1
        }
        throw new Error("Unknown Bean! dynamic@demo.Module1.AutoValue:ListDynamic");
    }

    public static CreateBeanFromSpecialTypeId_4(typeId: bigint): Zeze.Bean {
        switch (typeId)
        {
            case -6540229162696180765n: return new demo_Module1_Simple();
            case -410057899348847631n: return new demo_Bean1();
        }
        return null;
    }

    public static GetSpecialTypeIdFromBean_5(bean: Zeze.Bean): bigint {
        switch (bean.TypeId())
        {
            case Zeze.EmptyBean.TYPEID: return Zeze.EmptyBean.TYPEID;
            case -6540229162696180765n: return -6540229162696180765n; // demo.Module1.Simple
            case -410057899348847631n: return -410057899348847631n; // demo.Bean1
        }
        throw new Error("Unknown Bean! dynamic@demo.Module1.AutoValue:MapDynamic");
    }

    public static CreateBeanFromSpecialTypeId_5(typeId: bigint): Zeze.Bean {
        switch (typeId)
        {
            case -6540229162696180765n: return new demo_Module1_Simple();
            case -410057899348847631n: return new demo_Bean1();
        }
        return null;
    }

}

export class demo_Module1_Food implements Zeze.Bean {
    public Subclass: Zeze.DynamicBean; 


    public constructor() {
        this.Subclass = new Zeze.DynamicBean(demo_Module1_Food.GetSpecialTypeIdFromBean_14, demo_Module1_Food.CreateBeanFromSpecialTypeId_14);
    }


    public static readonly TYPEID: bigint = 4429157588515811655n;
    public TypeId(): bigint { return demo_Module1_Food.TYPEID; }

    public Encode(_o_: Zeze.ByteBuffer) {
        var _i_ = 0;
        {
            var _xd_ = this.Subclass;
            if (!_xd_.isEmpty()) {
                _i_ = _o_.WriteTag(_i_, 14, Zeze.ByteBuffer.DYNAMIC);
                _xd_.Encode(_o_);
            }
        }
        _o_.WriteByte(0);
    }

    public Decode(_o_: Zeze.ByteBuffer) {
        var _t_ = _o_.ReadByte();
        var _i_ = _o_.ReadTagSize(_t_);
        while (_t_ != 0 && _i_ < 14) {
            _o_.SkipUnknownField(_t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        if (_i_ == 14) {
            _o_.ReadDynamic(this.Subclass, _t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        while (_t_ != 0) {
            _o_.SkipUnknownField(_t_);
            _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
    }
    public static readonly DynamicTypeId_Subclass_demo_Bean1 : bigint = 1n;
    public static readonly DynamicTypeId_Subclass_demo_Module1_Simple : bigint = 2n;

    public static GetSpecialTypeIdFromBean_14(bean: Zeze.Bean): bigint {
        switch (bean.TypeId())
        {
            case Zeze.EmptyBean.TYPEID: return Zeze.EmptyBean.TYPEID;
            case -410057899348847631n: return 1n; // demo.Bean1
            case -6540229162696180765n: return 2n; // demo.Module1.Simple
        }
        throw new Error("Unknown Bean! dynamic@demo.Module1.Food:Subclass");
    }

    public static CreateBeanFromSpecialTypeId_14(typeId: bigint): Zeze.Bean {
        switch (typeId)
        {
            case 1n: return new demo_Bean1();
            case 2n: return new demo_Module1_Simple();
        }
        return null;
    }

}

export class demo_Module1_Item implements Zeze.Bean {
    public Subclass: Zeze.DynamicBean; 


    public constructor() {
        this.Subclass = new Zeze.DynamicBean(demo_Module1_Item.GetSpecialTypeIdFromBean_14, demo_Module1_Item.CreateBeanFromSpecialTypeId_14);
    }


    public static readonly TYPEID: bigint = 6478795823423002674n;
    public TypeId(): bigint { return demo_Module1_Item.TYPEID; }

    public Encode(_o_: Zeze.ByteBuffer) {
        var _i_ = 0;
        {
            var _xd_ = this.Subclass;
            if (!_xd_.isEmpty()) {
                _i_ = _o_.WriteTag(_i_, 14, Zeze.ByteBuffer.DYNAMIC);
                _xd_.Encode(_o_);
            }
        }
        _o_.WriteByte(0);
    }

    public Decode(_o_: Zeze.ByteBuffer) {
        var _t_ = _o_.ReadByte();
        var _i_ = _o_.ReadTagSize(_t_);
        while (_t_ != 0 && _i_ < 14) {
            _o_.SkipUnknownField(_t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        if (_i_ == 14) {
            _o_.ReadDynamic(this.Subclass, _t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        while (_t_ != 0) {
            _o_.SkipUnknownField(_t_);
            _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
    }
    public static readonly DynamicTypeId_Subclass_demo_Bean1 : bigint = 1n;
    public static readonly DynamicTypeId_Subclass_demo_Module1_Simple : bigint = 2n;
    public static readonly DynamicTypeId_Subclass_demo_Module1_Food : bigint = 3n;

    public static GetSpecialTypeIdFromBean_14(bean: Zeze.Bean): bigint {
        switch (bean.TypeId())
        {
            case Zeze.EmptyBean.TYPEID: return Zeze.EmptyBean.TYPEID;
            case -410057899348847631n: return 1n; // demo.Bean1
            case -6540229162696180765n: return 2n; // demo.Module1.Simple
            case 4429157588515811655n: return 3n; // demo.Module1.Food
        }
        throw new Error("Unknown Bean! dynamic@demo.Module1.Item:Subclass");
    }

    public static CreateBeanFromSpecialTypeId_14(typeId: bigint): Zeze.Bean {
        switch (typeId)
        {
            case 1n: return new demo_Bean1();
            case 2n: return new demo_Module1_Simple();
            case 3n: return new demo_Module1_Food();
        }
        return null;
    }

}

export class demo_Module1_Removed2 implements Zeze.Bean {
    public int_1: number;  // com aa


    public constructor() {
    }


    public static readonly TYPEID: bigint = 3523573186496138611n;
    public TypeId(): bigint { return demo_Module1_Removed2.TYPEID; }

    public Encode(_o_: Zeze.ByteBuffer) {
        var _i_ = 0;
        {
            var _xn_ = this.int_1;
            if (_xn_ != 0) {
                _i_ = _o_.WriteTag(_i_, 1, Zeze.ByteBuffer.INTEGER);
                _o_.WriteInt(_xn_);
            }
        }
        _o_.WriteByte(0);
    }

    public Decode(_o_: Zeze.ByteBuffer) {
        var _t_ = _o_.ReadByte();
        var _i_ = _o_.ReadTagSize(_t_);
        if (_i_ == 1) {
            this.int_1 = _o_.ReadIntT(_t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        while (_t_ != 0) {
            _o_.SkipUnknownField(_t_);
            _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
    }
}

export class demo_Module1_Simple implements Zeze.Bean {
    public int_1: number;  // com aa
    public long2: bigint;  // com aa
    public string3: string;  // com aa
    public removed: demo_Module1_Removed2;  // com aa
    public AllowCircle: Array<demo_Module1_Simple>;  // com aa


    public constructor() {
        this.long2 = 0n;
        this.string3 = "";
        this.removed = new demo_Module1_Removed2();
        this.AllowCircle = new Array<demo_Module1_Simple>();
    }


    public static readonly TYPEID: bigint = -6540229162696180765n;
    public TypeId(): bigint { return demo_Module1_Simple.TYPEID; }

    public Encode(_o_: Zeze.ByteBuffer) {
        var _i_ = 0;
        {
            var _xn_ = this.int_1;
            if (_xn_ != 0) {
                _i_ = _o_.WriteTag(_i_, 1, Zeze.ByteBuffer.INTEGER);
                _o_.WriteInt(_xn_);
            }
        }
        {
            var _xl_ = this.long2;
            if (_xl_ != 0n) {
                _i_ = _o_.WriteTag(_i_, 2, Zeze.ByteBuffer.INTEGER);
                _o_.WriteLong(_xl_);
            }
        }
        {
            var _xs_ = this.string3;
            if (_xs_.length != 0) {
                _i_ = _o_.WriteTag(_i_, 3, Zeze.ByteBuffer.BYTES);
                _o_.WriteString(_xs_);
            }
        }
        {
            var _a_ = _o_.WriteIndex;
            var _j_ = _o_.WriteTag(_i_, 4, Zeze.ByteBuffer.BEAN);
            var _b_ = _o_.WriteIndex;
            this.removed.Encode(_o_);
            if (_b_ + 1 == _o_.WriteIndex)
                _o_.WriteIndex = _a_;
            else
                _i_ = _j_;
        }
        {
            var _x5_ = this.AllowCircle;
            var _n_ = _x5_.length;
            if (_n_ != 0) {
                _i_ = _o_.WriteTag(_i_, 5, Zeze.ByteBuffer.LIST);
                _o_.WriteListType(_n_, Zeze.ByteBuffer.BEAN);
                for (var _v_ in _x5_) {
                    this.AllowCircle[_v_].Encode(_o_);
                }
            }
        }
        _o_.WriteByte(0);
    }

    public Decode(_o_: Zeze.ByteBuffer) {
        var _t_ = _o_.ReadByte();
        var _i_ = _o_.ReadTagSize(_t_);
        if (_i_ == 1) {
            this.int_1 = _o_.ReadIntT(_t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        if (_i_ == 2) {
            this.long2 = _o_.ReadLongT(_t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        if (_i_ == 3) {
            this.string3 = _o_.ReadStringT(_t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        if (_i_ == 4) {
            _o_.ReadBean(this.removed, _t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        if (_i_ == 5) {
            var _x5_ = new Array<demo_Module1_Simple>();
            this.AllowCircle = _x5_;
            if ((_t_ & Zeze.ByteBuffer.TAG_MASK) == Zeze.ByteBuffer.LIST)
            {
                for (var _n_ = _o_.ReadTagSize(_t_ = _o_.ReadByte()); _n_ > 0; _n_--)
                {
                    _x5_.push(_o_.ReadBean(new demo_Module1_Simple(), _t_));
                }
            }
            else
                _o_.SkipUnknownField(_t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        while (_t_ != 0) {
            _o_.SkipUnknownField(_t_);
            _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
    }
}

export class demo_Module1_Value implements Zeze.Bean {
    public static readonly Enum1 = 4;

    public int_1: number;  // com aa
    public long2: bigint;  // com aa
    public string3: string;  // com aa
    public bool4: boolean;  // com aa
    public short5: number;  // com aa
    public float6: number;  // com aa
    public double7: number;  // com aa
    public bytes8: Uint8Array;  // com aa
    public list9: Array<demo_Bean1>;  // com aa
    public set10: Set<number>;  // com aa
    public map11: Map<bigint, demo_Module2_Value>;  // com aa
    public bean12: demo_Module1_Simple;  // simple
    public byte13: number;  // com aa
    public dynamic14: Zeze.DynamicBean; 
    public map15: Map<bigint, bigint>;  // com aa
    public map16: Map<demo_Module1_Key, demo_Module1_Simple>;  // com aa
    public map17: Map<demo_Module1_Key, demo_Module1_Simple>; 
    public map18: Map<demo_Module1_Key, Zeze.DynamicBean>; 
    public dynamic19: Zeze.DynamicBean; 
    public version: bigint; 


    public constructor() {
        this.long2 = 0n;
        this.string3 = "";
        this.bytes8 = new Uint8Array(0);
        this.list9 = new Array<demo_Bean1>();
        this.set10 = new Set<number>();
        this.map11 = new Map<bigint, demo_Module2_Value>();
        this.bean12 = new demo_Module1_Simple();
        this.dynamic14 = new Zeze.DynamicBean(demo_Module1_Value.GetSpecialTypeIdFromBean_14, demo_Module1_Value.CreateBeanFromSpecialTypeId_14);
        this.map15 = new Map<bigint, bigint>();
        this.map16 = new Map<demo_Module1_Key, demo_Module1_Simple>();
        this.map17 = new Map<demo_Module1_Key, demo_Module1_Simple>();
        this.map18 = new Map<demo_Module1_Key, Zeze.DynamicBean>();
        this.dynamic19 = new Zeze.DynamicBean(demo_Module1_Value.GetSpecialTypeIdFromBean_19, demo_Module1_Value.CreateBeanFromSpecialTypeId_19);
        this.version = 0n;
    }


    public static readonly TYPEID: bigint = -6704978169579361478n;
    public TypeId(): bigint { return demo_Module1_Value.TYPEID; }

    public Encode(_o_: Zeze.ByteBuffer) {
        var _i_ = 0;
        {
            var _xn_ = this.int_1;
            if (_xn_ != 0) {
                _i_ = _o_.WriteTag(_i_, 1, Zeze.ByteBuffer.INTEGER);
                _o_.WriteInt(_xn_);
            }
        }
        {
            var _xl_ = this.long2;
            if (_xl_ != 0n) {
                _i_ = _o_.WriteTag(_i_, 2, Zeze.ByteBuffer.INTEGER);
                _o_.WriteLong(_xl_);
            }
        }
        {
            var _xs_ = this.string3;
            if (_xs_.length != 0) {
                _i_ = _o_.WriteTag(_i_, 3, Zeze.ByteBuffer.BYTES);
                _o_.WriteString(_xs_);
            }
        }
        {
            var _xb_ = this.bool4;
            if (_xb_) {
                _i_ = _o_.WriteTag(_i_, 4, Zeze.ByteBuffer.INTEGER);
                _o_.WriteByte(1);
            }
        }
        {
            var _xn_ = this.short5;
            if (_xn_ != 0) {
                _i_ = _o_.WriteTag(_i_, 5, Zeze.ByteBuffer.INTEGER);
                _o_.WriteInt(_xn_);
            }
        }
        {
            var _xn_ = this.float6;
            if (_xn_ != 0) {
                _i_ = _o_.WriteTag(_i_, 6, Zeze.ByteBuffer.FLOAT);
                _o_.WriteFloat(_xn_);
            }
        }
        {
            var _xn_ = this.double7;
            if (_xn_ != 0) {
                _i_ = _o_.WriteTag(_i_, 7, Zeze.ByteBuffer.DOUBLE);
                _o_.WriteDouble(_xn_);
            }
        }
        {
            var _xa_ = this.bytes8;
            if (_xa_.length != 0) {
                _i_ = _o_.WriteTag(_i_, 8, Zeze.ByteBuffer.BYTES);
                _o_.WriteBytes(_xa_);
            }
        }
        {
            var _x9_ = this.list9;
            var _n_ = _x9_.length;
            if (_n_ != 0) {
                _i_ = _o_.WriteTag(_i_, 9, Zeze.ByteBuffer.LIST);
                _o_.WriteListType(_n_, Zeze.ByteBuffer.BEAN);
                for (var _v_ in _x9_) {
                    this.list9[_v_].Encode(_o_);
                }
            }
        }
        {
            var _x10_ = this.set10;
            var _n_ = _x10_.size;
            if (_n_ != 0) {
                _i_ = _o_.WriteTag(_i_, 10, Zeze.ByteBuffer.LIST);
                _o_.WriteListType(_n_, Zeze.ByteBuffer.INTEGER);
                for (let _v_ of _x10_) {
                    _o_.WriteInt(_v_);
                }
            }
        }
        {
            var _x11_ = this.map11;
            var _n_ = _x11_.size;
            if (_n_ != 0) {
                _i_ = _o_.WriteTag(_i_, 11, Zeze.ByteBuffer.MAP);
                _o_.WriteMapType(_n_, Zeze.ByteBuffer.INTEGER, Zeze.ByteBuffer.BEAN);
                for (let _e_ of _x11_.entries()) {
                    _o_.WriteLong(_e_[0]);
                    _e_[1].Encode(_o_);
                }
            }
        }
        {
            var _a_ = _o_.WriteIndex;
            var _j_ = _o_.WriteTag(_i_, 12, Zeze.ByteBuffer.BEAN);
            var _b_ = _o_.WriteIndex;
            this.bean12.Encode(_o_);
            if (_b_ + 1 == _o_.WriteIndex)
                _o_.WriteIndex = _a_;
            else
                _i_ = _j_;
        }
        {
            var _xn_ = this.byte13;
            if (_xn_ != 0) {
                _i_ = _o_.WriteTag(_i_, 13, Zeze.ByteBuffer.INTEGER);
                _o_.WriteInt(_xn_);
            }
        }
        {
            var _xd_ = this.dynamic14;
            if (!_xd_.isEmpty()) {
                _i_ = _o_.WriteTag(_i_, 14, Zeze.ByteBuffer.DYNAMIC);
                _xd_.Encode(_o_);
            }
        }
        {
            var _x15_ = this.map15;
            var _n_ = _x15_.size;
            if (_n_ != 0) {
                _i_ = _o_.WriteTag(_i_, 15, Zeze.ByteBuffer.MAP);
                _o_.WriteMapType(_n_, Zeze.ByteBuffer.INTEGER, Zeze.ByteBuffer.INTEGER);
                for (let _e_ of _x15_.entries()) {
                    _o_.WriteLong(_e_[0]);
                    _o_.WriteLong(_e_[1]);
                }
            }
        }
        {
            var _x16_ = this.map16;
            var _n_ = _x16_.size;
            if (_n_ != 0) {
                _i_ = _o_.WriteTag(_i_, 16, Zeze.ByteBuffer.MAP);
                _o_.WriteMapType(_n_, Zeze.ByteBuffer.BEAN, Zeze.ByteBuffer.BEAN);
                for (let _e_ of _x16_.entries()) {
                    _e_[0].Encode(_o_);
                    _e_[1].Encode(_o_);
                }
            }
        }
        {
            var _x17_ = this.map17;
            var _n_ = _x17_.size;
            if (_n_ != 0) {
                _i_ = _o_.WriteTag(_i_, 17, Zeze.ByteBuffer.MAP);
                _o_.WriteMapType(_n_, Zeze.ByteBuffer.BEAN, Zeze.ByteBuffer.BEAN);
                for (let _e_ of _x17_.entries()) {
                    _e_[0].Encode(_o_);
                    _e_[1].Encode(_o_);
                }
            }
        }
        {
            var _x18_ = this.map18;
            var _n_ = _x18_.size;
            if (_n_ != 0) {
                _i_ = _o_.WriteTag(_i_, 18, Zeze.ByteBuffer.MAP);
                _o_.WriteMapType(_n_, Zeze.ByteBuffer.BEAN, Zeze.ByteBuffer.DYNAMIC);
                for (let _e_ of _x18_.entries()) {
                    _e_[0].Encode(_o_);
                    _e_[1].Encode(_o_);
                }
            }
        }
        {
            var _xd_ = this.dynamic19;
            if (!_xd_.isEmpty()) {
                _i_ = _o_.WriteTag(_i_, 19, Zeze.ByteBuffer.DYNAMIC);
                _xd_.Encode(_o_);
            }
        }
        {
            var _xl_ = this.version;
            if (_xl_ != 0n) {
                _i_ = _o_.WriteTag(_i_, 20, Zeze.ByteBuffer.INTEGER);
                _o_.WriteLong(_xl_);
            }
        }
        _o_.WriteByte(0);
    }

    public Decode(_o_: Zeze.ByteBuffer) {
        var _t_ = _o_.ReadByte();
        var _i_ = _o_.ReadTagSize(_t_);
        if (_i_ == 1) {
            this.int_1 = _o_.ReadIntT(_t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        if (_i_ == 2) {
            this.long2 = _o_.ReadLongT(_t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        if (_i_ == 3) {
            this.string3 = _o_.ReadStringT(_t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        if (_i_ == 4) {
            this.bool4 = _o_.ReadBoolT(_t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        if (_i_ == 5) {
            this.short5 = _o_.ReadIntT(_t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        if (_i_ == 6) {
            this.float6 = _o_.ReadFloatT(_t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        if (_i_ == 7) {
            this.double7 = _o_.ReadDoubleT(_t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        if (_i_ == 8) {
            this.bytes8 = _o_.ReadBytesT(_t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        if (_i_ == 9) {
            var _x9_ = new Array<demo_Bean1>();
            this.list9 = _x9_;
            if ((_t_ & Zeze.ByteBuffer.TAG_MASK) == Zeze.ByteBuffer.LIST)
            {
                for (var _n_ = _o_.ReadTagSize(_t_ = _o_.ReadByte()); _n_ > 0; _n_--)
                {
                    _x9_.push(_o_.ReadBean(new demo_Bean1(), _t_));
                }
            }
            else
                _o_.SkipUnknownField(_t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        if (_i_ == 10) {
            var _x10_ = this.set10;
            _x10_.clear();
            if ((_t_ & Zeze.ByteBuffer.TAG_MASK) == Zeze.ByteBuffer.LIST)
            {
                for (var _n_ = _o_.ReadTagSize(_t_ = _o_.ReadByte()); _n_ > 0; _n_--)
                {
                    _x10_.add(_o_.ReadIntT(_t_));
                }
            }
            else
                _o_.SkipUnknownField(_t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        if (_i_ == 11) {
            var _x11_ = this.map11;
            _x11_.clear();
            if ((_t_ & Zeze.ByteBuffer.TAG_MASK) == Zeze.ByteBuffer.MAP) {
                var _s_ = (_t_ = _o_.ReadByte()) >> Zeze.ByteBuffer.TAG_SHIFT;
                for (var _n_ = _o_.ReadUInt(); _n_ > 0; _n_--) {
                    var _k11_ = _o_.ReadLongT(_s_);
                    var _v11_ = _o_.ReadBean(new demo_Module2_Value(), _t_);
                    _x11_.set(_k11_, _v11_);
                }
            } else
                _o_.SkipUnknownField(_t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        if (_i_ == 12) {
            _o_.ReadBean(this.bean12, _t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        if (_i_ == 13) {
            this.byte13 = _o_.ReadIntT(_t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        if (_i_ == 14) {
            _o_.ReadDynamic(this.dynamic14, _t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        if (_i_ == 15) {
            var _x15_ = this.map15;
            _x15_.clear();
            if ((_t_ & Zeze.ByteBuffer.TAG_MASK) == Zeze.ByteBuffer.MAP) {
                var _s_ = (_t_ = _o_.ReadByte()) >> Zeze.ByteBuffer.TAG_SHIFT;
                for (var _n_ = _o_.ReadUInt(); _n_ > 0; _n_--) {
                    var _k15_ = _o_.ReadLongT(_s_);
                    var _v15_ = _o_.ReadLongT(_t_);
                    _x15_.set(_k15_, _v15_);
                }
            } else
                _o_.SkipUnknownField(_t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        if (_i_ == 16) {
            var _x16_ = this.map16;
            _x16_.clear();
            if ((_t_ & Zeze.ByteBuffer.TAG_MASK) == Zeze.ByteBuffer.MAP) {
                var _s_ = (_t_ = _o_.ReadByte()) >> Zeze.ByteBuffer.TAG_SHIFT;
                for (var _n_ = _o_.ReadUInt(); _n_ > 0; _n_--) {
                    var _k16_ = _o_.ReadBean(new demo_Module1_Key(), _s_);
                    var _v16_ = _o_.ReadBean(new demo_Module1_Simple(), _t_);
                    _x16_.set(_k16_, _v16_);
                }
            } else
                _o_.SkipUnknownField(_t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        if (_i_ == 17) {
            var _x17_ = this.map17;
            _x17_.clear();
            if ((_t_ & Zeze.ByteBuffer.TAG_MASK) == Zeze.ByteBuffer.MAP) {
                var _s_ = (_t_ = _o_.ReadByte()) >> Zeze.ByteBuffer.TAG_SHIFT;
                for (var _n_ = _o_.ReadUInt(); _n_ > 0; _n_--) {
                    var _k17_ = _o_.ReadBean(new demo_Module1_Key(), _s_);
                    var _v17_ = _o_.ReadBean(new demo_Module1_Simple(), _t_);
                    _x17_.set(_k17_, _v17_);
                }
            } else
                _o_.SkipUnknownField(_t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        if (_i_ == 18) {
            var _x18_ = this.map18;
            _x18_.clear();
            if ((_t_ & Zeze.ByteBuffer.TAG_MASK) == Zeze.ByteBuffer.MAP) {
                var _s_ = (_t_ = _o_.ReadByte()) >> Zeze.ByteBuffer.TAG_SHIFT;
                for (var _n_ = _o_.ReadUInt(); _n_ > 0; _n_--) {
                    var _k18_ = _o_.ReadBean(new demo_Module1_Key(), _s_);
                    var _v18_ = new Zeze.DynamicBean(demo_Module1_Value.GetSpecialTypeIdFromBean_18, demo_Module1_Value.CreateBeanFromSpecialTypeId_18);
                    _o_.ReadDynamic(_v18_, _t_);
                    _x18_.set(_k18_, _v18_);
                }
            } else
                _o_.SkipUnknownField(_t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        if (_i_ == 19) {
            _o_.ReadDynamic(this.dynamic19, _t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        if (_i_ == 20) {
            this.version = _o_.ReadLongT(_t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        while (_t_ != 0) {
            _o_.SkipUnknownField(_t_);
            _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
    }
    public static readonly DynamicTypeId_Dynamic14_demo_Bean1 : bigint = 1n;
    public static readonly DynamicTypeId_Dynamic14_demo_Module1_Simple : bigint = 2n;

    public static GetSpecialTypeIdFromBean_14(bean: Zeze.Bean): bigint {
        switch (bean.TypeId())
        {
            case Zeze.EmptyBean.TYPEID: return Zeze.EmptyBean.TYPEID;
            case -410057899348847631n: return 1n; // demo.Bean1
            case -6540229162696180765n: return 2n; // demo.Module1.Simple
        }
        throw new Error("Unknown Bean! dynamic@demo.Module1.Value:dynamic14");
    }

    public static CreateBeanFromSpecialTypeId_14(typeId: bigint): Zeze.Bean {
        switch (typeId)
        {
            case 1n: return new demo_Bean1();
            case 2n: return new demo_Module1_Simple();
        }
        return null;
    }

    public static GetSpecialTypeIdFromBean_18(bean: Zeze.Bean): bigint {
        switch (bean.TypeId())
        {
            case Zeze.EmptyBean.TYPEID: return Zeze.EmptyBean.TYPEID;
            case -6540229162696180765n: return -6540229162696180765n; // demo.Module1.Simple
        }
        throw new Error("Unknown Bean! dynamic@demo.Module1.Value:map18");
    }

    public static CreateBeanFromSpecialTypeId_18(typeId: bigint): Zeze.Bean {
        switch (typeId)
        {
            case -6540229162696180765n: return new demo_Module1_Simple();
        }
        return null;
    }

    public static readonly DynamicTypeId_Dynamic19_demo_Module1_Simple : bigint = -6540229162696180765n;

    public static GetSpecialTypeIdFromBean_19(bean: Zeze.Bean): bigint {
        switch (bean.TypeId())
        {
            case Zeze.EmptyBean.TYPEID: return Zeze.EmptyBean.TYPEID;
            case -6540229162696180765n: return -6540229162696180765n; // demo.Module1.Simple
        }
        throw new Error("Unknown Bean! dynamic@demo.Module1.Value:dynamic19");
    }

    public static CreateBeanFromSpecialTypeId_19(typeId: bigint): Zeze.Bean {
        switch (typeId)
        {
            case -6540229162696180765n: return new demo_Module1_Simple();
        }
        return null;
    }

}

export class demo_Module2_Value implements Zeze.Bean {
    public s: number;  // com aa


    public constructor() {
        this.s = 1;
    }


    public static readonly TYPEID: bigint = 6342134639811561033n;
    public TypeId(): bigint { return demo_Module2_Value.TYPEID; }

    public Encode(_o_: Zeze.ByteBuffer) {
        var _i_ = 0;
        {
            var _xn_ = this.s;
            if (_xn_ != 0) {
                _i_ = _o_.WriteTag(_i_, 1, Zeze.ByteBuffer.INTEGER);
                _o_.WriteInt(_xn_);
            }
        }
        _o_.WriteByte(0);
    }

    public Decode(_o_: Zeze.ByteBuffer) {
        var _t_ = _o_.ReadByte();
        var _i_ = _o_.ReadTagSize(_t_);
        if (_i_ == 1) {
            this.s = _o_.ReadIntT(_t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        while (_t_ != 0) {
            _o_.SkipUnknownField(_t_);
            _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
    }
}

export class demo_Module1_AutoKey implements Zeze.Bean {
    public Name: string;  // 一般就是表名。
    public LocalId: bigint; 

     public constructor(_Name_: string = "", _LocalId_: bigint = 0n) {
        this.Name = _Name_;
        this.LocalId = _LocalId_;
    }

    public static readonly TYPEID: bigint = -9163995489891467717n;
    public TypeId(): bigint { return demo_Module1_AutoKey.TYPEID; }


    public Encode(_o_: Zeze.ByteBuffer) {
        var _i_ = 0;
        {
            var _xs_ = this.Name;
            if (_xs_.length != 0) {
                _i_ = _o_.WriteTag(_i_, 1, Zeze.ByteBuffer.BYTES);
                _o_.WriteString(_xs_);
            }
        }
        {
            var _xl_ = this.LocalId;
            if (_xl_ != 0n) {
                _i_ = _o_.WriteTag(_i_, 2, Zeze.ByteBuffer.INTEGER);
                _o_.WriteLong(_xl_);
            }
        }
        _o_.WriteByte(0);
    }

    public Decode(_o_: Zeze.ByteBuffer) {
        var _t_ = _o_.ReadByte();
        var _i_ = _o_.ReadTagSize(_t_);
        if (_i_ == 1) {
            this.Name = _o_.ReadStringT(_t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        if (_i_ == 2) {
            this.LocalId = _o_.ReadLongT(_t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        while (_t_ != 0) {
            _o_.SkipUnknownField(_t_);
            _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
    }
}
export class demo_Module1_Key implements Zeze.Bean {
    public static readonly Enum1 = 4;

    public s: number;  // com 2			com 2

     public constructor(_s_: number = 1) {
        this.s = _s_;
    }

    public static readonly TYPEID: bigint = -4033798796484741924n;
    public TypeId(): bigint { return demo_Module1_Key.TYPEID; }


    public Encode(_o_: Zeze.ByteBuffer) {
        var _i_ = 0;
        {
            var _xn_ = this.s;
            if (_xn_ != 0) {
                _i_ = _o_.WriteTag(_i_, 1, Zeze.ByteBuffer.INTEGER);
                _o_.WriteInt(_xn_);
            }
        }
        _o_.WriteByte(0);
    }

    public Decode(_o_: Zeze.ByteBuffer) {
        var _t_ = _o_.ReadByte();
        var _i_ = _o_.ReadTagSize(_t_);
        if (_i_ == 1) {
            this.s = _o_.ReadIntT(_t_);
            _i_ += _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
        while (_t_ != 0) {
            _o_.SkipUnknownField(_t_);
            _o_.ReadTagSize(_t_ = _o_.ReadByte());
        }
    }
}
export class demo_Module1_Protocol1 extends Zeze.ProtocolWithArgument<demo_Module1_Value> {
    public ModuleId(): number { return 1; }
    public ProtocolId(): number { return -1219587236; }

    public constructor() {
        super(new demo_Module1_Value());
    }
}
export class demo_Module1_Protocol3 extends Zeze.ProtocolWithArgument<demo_Module2_Value> {
    public ModuleId(): number { return 1; }
    public ProtocolId(): number { return -774467372; }

    public constructor() {
        super(new demo_Module2_Value());
    }
}
export class demo_Module1_ProtocolNoProcedure extends Zeze.ProtocolWithArgument<Zeze.EmptyBean> {
    public ModuleId(): number { return 1; }
    public ProtocolId(): number { return 1350939829; }

    public constructor() {
        super(new Zeze.EmptyBean());
    }
}
export class demo_Module1_Rpc1 extends Zeze.Rpc<demo_Module1_Value, demo_Module1_Value> {
    public ModuleId(): number { return 1; }
    public ProtocolId(): number { return 1340115327; }
    public constructor() {
        super(new demo_Module1_Value(), new demo_Module1_Value());
    }
}
export class demo_Module1_Rpc2 extends Zeze.Rpc<demo_Module1_Value, demo_Module1_Value> {
    public ModuleId(): number { return 1; }
    public ProtocolId(): number { return -735856552; }
    public constructor() {
        super(new demo_Module1_Value(), new demo_Module1_Value());
    }
}
