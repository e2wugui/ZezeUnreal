
// ZEZE_FILE_CHUNK {{{ IMPORT GEN
import { Zeze } from '../../Zeze/zeze';
import { demo_Module1_Protocol1, demo_Module1_Protocol3, demo_Module1_Rpc1, demo_Module1_Rpc2 } from '../gen';
import App from '../App';
// ZEZE_FILE_CHUNK }}} IMPORT GEN

export class demo_Module1 {
    public constructor(app: demo_App) {
        // ZEZE_FILE_CHUNK {{{ REGISTER PROTOCOL
        app.Client.FactoryHandleMap.set(7370347356n, new Zeze.ProtocolFactoryHandle(
            () => { return new demo_Module1_Protocol1(); },
            p => this.ProcessProtocol1(<demo_Module1_Protocol1>p)));
        app.Client.FactoryHandleMap.set(7815467220n, new Zeze.ProtocolFactoryHandle(
            () => { return new demo_Module1_Protocol3(); },
            p => this.ProcessProtocol3(<demo_Module1_Protocol3>p)));
        app.Client.FactoryHandleMap.set(5635082623n, new Zeze.ProtocolFactoryHandle(
            () => { return new demo_Module1_Rpc1(); },
            null));
        app.Client.FactoryHandleMap.set(7854078040n, new Zeze.ProtocolFactoryHandle(
            () => { return new demo_Module1_Rpc2(); },
            p => this.ProcessRpc2Request(<demo_Module1_Rpc2>p)));
        // ZEZE_FILE_CHUNK }}} REGISTER PROTOCOL
    }

    public Start(app: demo_App): void {
    }

    public Stop(app: demo_App): void {
    }

    public ProcessRpc2Request(rpc: demo_Module1_Rpc2): number {
        console.log('Rpc.Request: demo_Module1_Rpc2');
        rpc.SendResult();
        return 0;
    }

    public ProcessProtocol1(protocol: demo_Module1_Protocol1): number {
        console.log("ProcessProtocol1");
        new demo_Module1_Rpc1().SendWithCallback(protocol.Sender, (rpc: demo_Module1_Rpc1) => {
            console.log('Rpc.Response: demo_Module1_Rpc1');
            return 0;
        }
        );
        return 0;
    }

    public ProcessProtocol3(protocol: demo_Module1_Protocol3): number {
        return 0;
    }

}
                                                                                                                                                                                                                                                                                                          
