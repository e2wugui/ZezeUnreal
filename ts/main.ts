
import * as UE from 'ue'
import { demo_App } from "demo/App"
import { Zeze } from "zeze"
import { demo_Module1_Protocol1 } from "gen"

class ServiceEventHandle implements Zeze.IServiceEventHandle {
    OnSocketConnected(service: Zeze.Service, socket: Zeze.Socket): void {
        console.log("OnSocketConnected");
        new demo_Module1_Protocol1().Send(socket);
    }

    OnSocketClosed(service: Zeze.Service, socket: Zeze.Socket): void {
        console.log("OnSocketClosed");
    }

    OnSoekctInput(service: Zeze.Service, socket: Zeze.Socket, buffer: ArrayBuffer, offset: number, len: number): boolean {
        return false;
    }
}

let app = new demo_App();
app.Client.ServiceEventHandle = new ServiceEventHandle();
app.Start();
console.log("Connect Now");
app.Client.Connect("127.0.0.1", 9999, true);

setInterval(() => { app.Client.TickUpdate(); }, 10);
